#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <libudev.h>

#include "mm_internal.h"

unsigned char *mm_out_report, *mm_in_report, *mm_in_reports;
long buffers;
int fd;

int initDataStructures()
{
  if (mm_in_reports)
    free(mm_in_reports);
  if (mm_in_report)
    free(mm_in_report);
  if (mm_out_report)
    free(mm_out_report);
  mm_in_reports = calloc(buffers, 7);
  mm_in_report = calloc(7, 1);
  mm_out_report = calloc(9, 1);
  if (!mm_in_report)
    return 1;
  if (!mm_out_report)
    return 1;
  mm_out_report[7] = 1;
  return 0;
}

int minimaid_open_device()
{
  int ret;
  char path[264];

  ret = findMinimaidUdev(path);
  if (ret == 1)
  {
    return 1;
  }
  fd = open(path, 0x802);
  if (fd < 0)
  {
    perror("Unable to open minimaid");
    return 1;
  }

  if (((initDataStructures() == 1) || (mm_in_report == NULL)) ||
      (mm_out_report == NULL))
  {
    close(fd);
    fd = -1;
    return 1;
  }
  return 0;
}

int findMinimaidUdev(char *node_path)

{
  int result;
  int cmp_result;
  struct udev *udev;
  struct udev_enumerate *udev_enumerate;
  struct udev_device *udev_device;
  struct udev_device *udev_device_parent;
  const char *syspath;
  const char *node_name;
  const char *vendor_id;
  const char *product_id;
  int index = 0;
  struct udev_list_entry *list_entry;

  if ((udev = udev_new()) == NULL)
  {
    puts("Can\'t create udev");
  }
  else
  {
    udev_enumerate = udev_enumerate_new(udev);
    udev_enumerate_add_match_subsystem(udev_enumerate, "hidraw");
    udev_enumerate_scan_devices(udev_enumerate);
    list_entry = udev_enumerate_get_list_entry(udev_enumerate);
    while (list_entry != NULL)
    {
      syspath = udev_list_entry_get_name(list_entry);
      udev_device = udev_device_new_from_syspath(udev, syspath);
      memset(node_path, 0, 8);
      node_name = udev_device_get_devnode(udev_device);
      strcpy(node_path, node_name);
      printf("Device Node Path: %s\n", node_path);
      udev_device_parent =
          udev_device_get_parent_with_subsystem_devtype(udev_device, "usb", "usb_device");
      if (udev_device_parent == NULL)
      {
        printf("Unable to find parent usb device.");
        exit(1);
      }
      vendor_id = udev_device_get_sysattr_value(udev_device_parent, "idVendor");
      result = strcmp("beef", vendor_id);
      if (result == 0)
      {
        product_id = udev_device_get_sysattr_value(udev_device_parent, "idProduct");
        cmp_result = strcmp("5730", product_id);
        if ((cmp_result == 0) && (++index > 1))
        {
          puts("Found minimaid device!");
          udev_device_unref(udev_device_parent);
          udev_enumerate_unref(udev_enumerate);
          udev_unref(udev);
          return 0;
        }
      }
      udev_device_unref(udev_device_parent);
      list_entry = udev_list_entry_get_next(list_entry);
    }
    udev_enumerate_unref(udev_enumerate);
    udev_unref(udev);
    memset(node_path, 0, 8);
  }
  return 1;
}
