project(mmmagic C)

add_library(mmmagic STATIC
    asyncthreads.c
    minimaidhid.c
    mm_internal.h
    mm_lowlevel.c
    mmmagic.c            
    mmmagic.h
    update.c            
)
