#include "mm_internal.h"

unsigned long mm_minimaid_UpdateInputs(void (*callback)(void *), void *args)
{
  unsigned long ret = minimaid_UpdateInputsWork();

  if (callback != NULL)
  {
    (*callback)(args);
  }
  return ret;
}

unsigned long mm_minimaid_SetOutputs(void (*callback)(void *), void *args)
{
  unsigned long ret = minimaid_SetOutputsWork();

  if (callback != NULL)
  {
    (*callback)(args);
  }
  return ret;
}
