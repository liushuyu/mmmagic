#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "mm_internal.h"

unsigned int reports_read;

ssize_t minimaid_UpdateInputsWork(void)
{
  ssize_t result;
  unsigned int read_count;
  unsigned char *cursor;

  if (mm_in_reports == NULL)
  {
    result = -1;
  }
  else
  {
    result = read(fd, mm_in_reports, 7);
    if (result < 0)
    {
      perror("read");
      result = -1;
    }
    else
    {
      printf("read() read %ld bytes:\n\t", result);
      cursor = mm_in_reports;
      memset(mm_in_report, 0, 7);
      read_count = 0;
      while (read_count < reports_read)
      {
        mm_in_report[1] = cursor[1] | mm_in_report[1];
        mm_in_report[2] = cursor[2] | mm_in_report[2];
        mm_in_report[3] = cursor[3] | mm_in_report[3];
        mm_in_report[4] = cursor[4] | mm_in_report[4];
        mm_in_report[5] = cursor[5] | mm_in_report[5];
        mm_in_report[6] = cursor[6] | mm_in_report[6];
        cursor += 7;
        read_count++;
      }
      result = reports_read;
    }
  }
  return result;
}

int minimaid_SetOutputsWork(void)
{
  int ret;
  ssize_t written;

  if (mm_out_report == NULL)
  {
    ret = -1;
  }
  else
  {
    written = write(fd, mm_out_report, 9);
    if (written < 0)
    {
      (void)errno;
      ret = -1;
    }
    else
    {
      ret = 0;
    }
  }
  return ret;
}
