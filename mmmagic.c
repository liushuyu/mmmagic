#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "mm_internal.h"

unsigned char __DDR_PAD1_LIGHTS, __DDR_PAD2_LIGHTS, __DDR_CABINET_LIGHTS, __DDR_BASS_LIGHTS, __BLUE_LED;
bool __mm;
bool __kb = true;

void mm_input_callback()
{
  return;
}

bool __bitValid(int bit)
{
  return bit >= 0 && bit <= 7;
}

unsigned char mm_turnON(unsigned char set, int bit)
{
  return BIT(bit) | set;
}

unsigned char mm_turnOFF(unsigned char set, int bit)
{
  return ~(1 << bit) & set;
}

void mm_setBlueLED(unsigned char value)
{
  __BLUE_LED = value;
  return;
}

void mm_setDDRPad1Light(int bit, int turn_on)
{
  if (__bitValid(bit))
  {
    if (turn_on == 1)
    {
      __DDR_PAD1_LIGHTS = mm_turnON(__DDR_PAD1_LIGHTS, bit);
    }
    if (turn_on == 0)
    {
      __DDR_PAD1_LIGHTS = mm_turnOFF(__DDR_PAD1_LIGHTS, bit);
    }
  }
  return;
}

void mm_setDDRPad2Light(int bit, int turn_on)
{
  if (__bitValid(bit))
  {
    if (turn_on == 1)
    {
      __DDR_PAD2_LIGHTS = mm_turnON(__DDR_PAD2_LIGHTS, bit);
    }
    if (turn_on == 0)
    {
      __DDR_PAD2_LIGHTS = mm_turnOFF(__DDR_PAD2_LIGHTS, bit);
    }
  }
  return;
}

void mm_setDDRCabinetLight(int bit, int turn_on)
{
  if (__bitValid(bit))
  {
    if (turn_on == 1)
    {
      __DDR_CABINET_LIGHTS = mm_turnON(__DDR_CABINET_LIGHTS, bit);
    }
    if (turn_on == 0)
    {
      __DDR_CABINET_LIGHTS = mm_turnOFF(__DDR_CABINET_LIGHTS, bit);
    }
  }
  return;
}

void mm_setDDRBassLight(int bit, int turn_on)
{
  if (__bitValid(bit))
  {
    if (turn_on == 1)
    {
      __DDR_BASS_LIGHTS = mm_turnON(__DDR_BASS_LIGHTS, bit);
      if (bit == 0)
      {
        mm_setBlueLED(0xff);
      }
    }
    if (turn_on == 0)
    {
      __DDR_BASS_LIGHTS = mm_turnOFF(__DDR_BASS_LIGHTS, bit);
      if (bit == 0)
      {
        mm_setBlueLED(0);
      }
    }
  }
  return;
}

void mm_setDDRAllOff()
{
  __DDR_CABINET_LIGHTS = 0;
  __DDR_BASS_LIGHTS = 0;
  __DDR_PAD1_LIGHTS = 0;
  __DDR_PAD2_LIGHTS = 0;
  mm_setBlueLED(0);
  return;
}

void mm_setDDRAllOn()
{
  __DDR_CABINET_LIGHTS = 0xff;
  __DDR_BASS_LIGHTS = 0xff;
  __DDR_PAD1_LIGHTS = 0xff;
  __DDR_PAD2_LIGHTS = 0xff;
  mm_setBlueLED(0xff);
  return;
}

bool mm_setKB(bool val)
{
  int result;

  if (__mm != false)
  {
    if (val == false)
    {
      mm_out_report[7] = 0;
    }
    else
    {
      mm_out_report[7] = 1;
    }
    __kb = (val != false);
    result = mm_minimaid_SetOutputs(0, 0);
    if (result == -1)
    {
      __mm = false;
      val = false;
    }
  }
  return val;
}

void mm_init()
{
  int result;

  if (__mm != false)
  {
    mm_out_report[2] = 0xff;
    mm_out_report[3] = 0xff;
    mm_out_report[4] = 0xff;
    mm_out_report[5] = 0xff;
    mm_out_report[6] = 0xff;
    mm_out_report[7] = 1;
    result = mm_minimaid_SetOutputs(0, 0);
    if (result == -1)
    {
      __mm = false;
    }
    else
    {
      mm_out_report[2] = 0;
      mm_out_report[3] = 0;
      mm_out_report[4] = 0;
      mm_out_report[5] = 0;
      mm_out_report[6] = 0;
      result = mm_minimaid_SetOutputs(0, 0);
      if (result == -1)
      {
        __mm = false;
      }
      else
      {
        mm_setKB(__kb);
      }
    }
  }
  return;
}

bool mm_connect_minimaid()
{
  int result;
  bool connected;

  result = minimaid_open_device();
  __mm = result != 0;
  connected = !__mm;
  if (connected)
  {
    puts("Minimaid connected!");
  }
  else
  {
    puts("Failed to connect to the minimaid.");
  }
  __mm = connected;
  mm_init();
  return __mm;
}

void mm_setMMOutputReports(unsigned char a, unsigned char b, unsigned char c, unsigned char d)
{
  if (__mm != false)
  {
    mm_out_report[2] = a;
    mm_out_report[3] = b;
    mm_out_report[4] = c;
    mm_out_report[5] = d;
    mm_out_report[6] = __BLUE_LED;
    if (__kb == false)
    {
      mm_out_report[7] = 0;
    }
    else
    {
      mm_out_report[7] = 1;
    }
  }
  return;
}

bool mm_sendDDRMiniMaidUpdate()
{
  bool ret;
  int result;

  mm_setDDRPad1Light(4, 1);
  mm_setDDRPad2Light(4, 1);
  if (__mm != true)
  {
    mm_connect_minimaid();
  }
  if (__mm == true)
  {
    mm_setMMOutputReports(__DDR_CABINET_LIGHTS, __DDR_PAD1_LIGHTS, __DDR_PAD2_LIGHTS, __DDR_BASS_LIGHTS);
    result = mm_minimaid_SetOutputs(0, 0);
    if (result == -1)
    {
      __mm = false;
      puts("Oh shit! The minimaid was found to be disconnected mid-transmit!");
      ret = false;
    }
    else
    {
      ret = true;
    }
  }
  else
  {
    ret = false;
  }
  return ret;
}
