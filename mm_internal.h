#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "mmmagic.h"

unsigned long mm_minimaid_UpdateInputs(void (*callback)(void *), void *args);
unsigned long mm_minimaid_SetOutputs(void (*callback)(void *), void *args);
int minimaid_open_device(void);
int findMinimaidUdev(char *node_path);
int initDataStructures(void);
ssize_t minimaid_UpdateInputsWork(void);
int minimaid_SetOutputsWork(void);

extern bool __mm, __kb;
extern long buffers;
extern int fd;
extern unsigned int reports_read;

extern unsigned char __DDR_PAD1_LIGHTS, __DDR_PAD2_LIGHTS, __DDR_CABINET_LIGHTS, __DDR_BASS_LIGHTS, __BLUE_LED,
    *mm_out_report, *mm_in_report, *mm_in_reports;
